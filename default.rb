#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'nginx' do
  action :install
    end

template 'nginx.conf' do
	path '/etc/nginx/nginx.conf'
	source 'nginx.conf.erb'
	end
	
service 'nginx' do
  action [ :enable, :restart ]
  end

cookbook_file "/usr/share/nginx/www/index.html" do
  source "index.html"
    mode "0644"
    end
